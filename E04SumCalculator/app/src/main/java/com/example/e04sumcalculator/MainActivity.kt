package com.example.e04sumcalculator

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }


    fun numberInput(view: View) {
        val result = findViewById<TextView>(R.id.result)
        // view is a button (pressed one) get text and convert to Int
        val digit = (view as Button).text.toString().toInt()
        result.append(digit.toString())
    }

    fun operationAction (view: View) {
        val result = findViewById<TextView>(R.id.result)
        if (view is Button) {
            val operator = view.text.toString()
            result.append(operator) // Assuming 'result' is a StringBuilder or TextView where you want to display the text
        }
    }

    fun allClear(view: View) {
        val result = findViewById<TextView>(R.id.result)
        val calculate = findViewById<TextView>(R.id.calculate)
        result.text = ""
        calculate.text = ""
    }

    fun equals(view: View) {
        val result = findViewById<TextView>(R.id.result)

    }


}


