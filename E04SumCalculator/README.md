# Exercise 04 - Sum Calculator

## Exercise description

The purpose of this exercise is to learn how to use Layout Editor and handle UI events. You will need to do an application, where end user can add numbers together - like a sum calculator. Remember check that your application UI is working with portrait and landscape orientations.

## Screenshots from the Android Emulator

<img src="./potrait.png" width='350'>

<img src="./landscape.png" width='600'>