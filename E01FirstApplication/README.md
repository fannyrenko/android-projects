# Exercise 01 - Create a first Android application
## Exercise description
The purpose of this tutorial is to learn how to create a project in Android Studio. You will learn how to configure project for the phone and the tablet – create different layout files to portrait and landscape mode and how to use resources to support different languages and localization. Created application will be tested with Android Studio emulators and/or with own device. In conclusion, this tutorial will give you good start to Android application development.

## Screenshots

### Kotlin basics completed

<img src='kotlin_basics_completed.PNG' width='600'>

### Screenshots from the Android Emulator

<img src='Screenshot_portrait_clickme.png' width='300'>

<img src='Screenshot_portrait_clicked.png' width='300'>

<img src='Screenshot_landscape_clickme.png' width='600'>

<img src='Screenshot_landscape._clicked.png' width='600'>