package com.example.e08employeesapp

import android.media.Image

data class Employee(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val email: String,
    val phone: String,
    val title: String,
    val department: String,
    val image: String
)