package com.example.e08employeesapp

import retrofit2.Call
import retrofit2.http.GET


interface ApiService {
    @GET("android_data.json")
    suspend fun getEmployees(): Call<List<Employee>>
}

