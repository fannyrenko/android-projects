package com.example.e08employeesapp

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import org.json.JSONArray
import org.json.JSONObject


class EmployeesAdapter(private var employees: JSONArray)
    : RecyclerView.Adapter<EmployeesAdapter.ViewHolder>() {

    // Create UI View Holder from XML layout
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.employee_item, parent, false)
        return ViewHolder(view)
    }

    // View Holder class to hold UI views
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val nameTextView: TextView = view.findViewById(R.id.nameTextView)
        val titleTextView: TextView = view.findViewById(R.id.titleTextView)
        val emailTextView: TextView = view.findViewById(R.id.emailTextView)
        val phoneTextView: TextView = view.findViewById(R.id.phoneTextView)
        val departmentTextView: TextView = view.findViewById(R.id.departmentTextView)
        val employeeImage : ImageView = view.findViewById(R.id.employeeImage)

        // add a item click listener
        init {
            itemView.setOnClickListener {
                Toast.makeText(
                    view.context,
                    "Employee name is ${nameTextView.text},adapter position = $adapterPosition",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        // Add a item click listener
        init {
            itemView.setOnClickListener {
                // remove or comment earlier Toast-message

                // create an explicit intent
                val intent = Intent(view.context, EmployeeActivity::class.java)
                // add selected employee JSON as a string data
                intent.putExtra("employee",employees[adapterPosition].toString())
                // start a new activity
                view.context.startActivity(intent)
            }
        }


    }




    // Bind data to UI View Holder
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int)
    {
        // employee to bind UI
        val employee: JSONObject = employees.getJSONObject(position)

        holder.nameTextView.text = employee["lastName"].toString()+" "+ employee["firstName"].toString()
        holder.titleTextView.text = "Title: ${employee.getString("title")}"
        holder.emailTextView.text = "Email: ${employee.getString("email")}"
        holder.phoneTextView.text = "Phone: ${employee.getString("phone")}"
        holder.departmentTextView.text = "Department: ${employee.getString("department")}"

        // Use Glide to load the employee's image
        Glide.with(holder.employeeImage.context)
            .load(employee.getString("image"))
            .placeholder(R.drawable.ic_person_placeholder)
            .into(holder.employeeImage)

    }

    // Return item count in employees
    override fun getItemCount(): Int = employees.length()

    // Method to update adapter data
    @SuppressLint("NotifyDataSetChanged")
    fun updateData(newData: JSONArray) {
        employees = newData
        notifyDataSetChanged()
    }


}