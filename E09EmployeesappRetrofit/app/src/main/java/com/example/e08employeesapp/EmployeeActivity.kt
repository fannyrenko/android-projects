package com.example.e08employeesapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.ComponentActivity
import com.bumptech.glide.Glide
import org.json.JSONObject

class EmployeeActivity : ComponentActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee)

        // get data from intent
        val bundle: Bundle? = intent.extras
        if (bundle != null) {
            val employeeString = bundle.getString("employee")
            if (employeeString != null) {
                val employee = JSONObject(employeeString)

                // Update the layout with employee data
                val employeeImage: ImageView = findViewById(R.id.employeeImage)
                val nameTextView: TextView = findViewById(R.id.nameTextView)
                val titleTextView: TextView = findViewById(R.id.titleTextView)
                val emailTextView: TextView = findViewById(R.id.emailTextView)
                val phoneTextView: TextView = findViewById(R.id.phoneTextView)
                val departmentTextView: TextView = findViewById(R.id.departmentTextView)

                // Assume that you have appropriate keys in your JSON, update them accordingly
                val name = employee.getString("lastName") + " " + employee.getString("firstName")
                val title = employee.getString("title")
                val email = employee.getString("email")
                val phone = employee.getString("phone")
                val department = employee.getString("department")
                val imageUrl = employee.getString("image")

                // Set data to views
                nameTextView.text = name
                titleTextView.text = "Title: $title"
                emailTextView.text = "Email: $email"
                phoneTextView.text = "Phone: $phone"
                departmentTextView.text = "Department: $department"

                // Use Glide to load the employee's image
                Glide.with(this@EmployeeActivity)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_person_placeholder)
                    .into(employeeImage)
            }
        }
    }
}

