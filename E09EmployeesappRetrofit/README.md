# Exercise 08 - Show employees in a RecyclerView

## Exercise description

Create a same kind of application what you did in exercise 08. Your application should load employees data from JSON file and display data in recycler view. Selected employee data is displayed in separate view. Look more requirements below images.


## Screenshots from the Android Emulator

### Viewholder

<img src="./viewholder.png" height='600'>

### Employees Activity Layout

<img src="./employeesActivityLayout.png" height='600'>