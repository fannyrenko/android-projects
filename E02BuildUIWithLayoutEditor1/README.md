# Exercise 02 - Build UI with Layout Editor 1

## Exercise description

The purpose of this exercise is to learn to use Layout Editor. You will add a few view objects to your UI and you need to position those to the different places in the screen. You need to check that your UI is working correctly in different devices and screen sizes.


## Screenshots from the Android Emulator

<img src="./screenshots/screenshot_portrait.png" width='350'/>

<img src="./screenshots/screenshot_landscape.png" alt="screenshot" width='600'/>


