# Exercise 11 - Show Golf Courses in a Google Maps

## Exercise description
In this exercise you will learn to use Google Maps in your Android application. With the Maps SDK for Android, you can add maps based on Google Maps data to your application. The API automatically handles access to Google Maps servers, data downloading, map display, and response to map gestures.

## Screenshots from Android Emulator
### Golf Courses in a Google Maps - Screenshot 1

<img src="./screenshot_1.png" height='600'>

### Golf Courses in a Google Maps - Screenshot 2

<img src="./screenshot_2.png" height='600'>

