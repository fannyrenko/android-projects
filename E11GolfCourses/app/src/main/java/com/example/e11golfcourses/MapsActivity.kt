package com.example.e11golfcourses

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.e11golfcourses.databinding.ActivityMapsBinding
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import org.json.JSONArray
import org.json.JSONException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        loadData()


    }

    private fun loadData() {
        val url = "https://ptm.fi/materials/golfcourses/golf_courses.json"
        val courseTypes: Map<String, Float> = mapOf(
            "?" to BitmapDescriptorFactory.HUE_VIOLET,
            "Etu" to BitmapDescriptorFactory.HUE_BLUE,
            "Kulta" to BitmapDescriptorFactory.HUE_GREEN,
            "Kulta/Etu" to BitmapDescriptorFactory.HUE_YELLOW
        )

        val queue = Volley.newRequestQueue(this)

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                try {
                    val golfCourses = response.getJSONArray("courses")

                    // loop through all objects
                    for (i in 0 until golfCourses.length()) {
                        // get course data
                        val course = golfCourses.getJSONObject(i)
                        val lat = course["lat"].toString().toDouble()
                        val lng = course["lng"].toString().toDouble()
                        val latLng = LatLng(lat, lng)
                        val type = course["type"].toString()
                        val title = course["course"].toString()
                        val address = course["address"].toString()
                        val phone = course["phone"].toString()
                        val email = course["email"].toString()
                        val webUrl = course["web"].toString()

                        if (courseTypes.containsKey(type)) {
                            val marker = mMap.addMarker(
                                MarkerOptions()
                                    .position(latLng)
                                    .title(title)
                                    .icon(BitmapDescriptorFactory.defaultMarker(courseTypes[type]!!))
                            )

                            // pass data to marker via Tag
                            val list = listOf(address, phone, email, webUrl)
                            if (marker != null) {
                                marker.tag = list
                            }
                        } else {
                            Log.d("GolfCourses", "This course type does not exist in evaluation $type")
                        }
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(65.5, 26.0), 5.0F))
                } catch (e: JSONException) {
                    Log.e("GolfCourses", "Error parsing JSON", e)
                }
            },
            { error ->
                // Error loading JSON
                Log.e("GolfCourses", "Error loading JSON", error)
            }
        )

        queue.add(jsonObjectRequest)

        // Set custom info window adapter
        mMap.setInfoWindowAdapter(CustomInfoWindowAdapter())

    }

    internal inner class CustomInfoWindowAdapter : GoogleMap.InfoWindowAdapter {
        private val contents: View = LayoutInflater.from(this@MapsActivity).inflate(R.layout.info_window, null)

        override fun getInfoWindow(marker: Marker): View? {
            return null
        }

        override fun getInfoContents(marker: Marker): View {
            val titleTextView = contents.findViewById<TextView>(R.id.titleTextView)
            val addressTextView = contents.findViewById<TextView>(R.id.addressTextView)
            val phoneTextView = contents.findViewById<TextView>(R.id.phoneTextView)
            val emailTextView = contents.findViewById<TextView>(R.id.emailTextView)
            val webTextView = contents.findViewById<TextView>(R.id.webTextView)

            titleTextView.text = marker.title.toString()

            if (marker.tag is List<*>) {
                val list: List<String> = marker.tag as List<String>
                addressTextView.text = list[0]
                phoneTextView.text = list[1]
                emailTextView.text = list[2]
                webTextView.text = list[3]
            }

            return contents
        }
    }

}