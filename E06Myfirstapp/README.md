# Exercise 06 - Build your first app with multiple activities

## Exercise description

The purpose of this exercise is to learn more about Activities (UI) and how to use Intents. Your target is to create a two activities.


## Screenshots from the Android Emulator
### Activity Main edit text and send button

<img src="./testing_mainActivity.png" width='300'>

### Activity Second received the message

<img src="./message_SecondActivity.png" width='300'>