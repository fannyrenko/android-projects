package com.example.e06myfirstapp

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val textView = findViewById<TextView>(R.id.recievedMessage)

        var intent = intent

        intent = getIntent()
        var intentText = intent.getStringExtra("Name")
        textView.text = intentText

    }

    fun goBack(view: View) {
        finish()
    }

}


