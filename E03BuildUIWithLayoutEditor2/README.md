# Exercise 03 - Build UI with Layout Editor 2

## Exercise description
The purpose of this tutorial is to learn to use Android Studio’s Layout Editor to create a fluid application UI. You will add a view layouts with a common components to display an employees data in UI. Designed application will run smoothly in different devices and screen sizes.

## Screenshots from the Android Emulator

<img src="./portrait.png" width='350'>

<img src="./landscape.png" width='600'>