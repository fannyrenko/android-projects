package com.example.e07myfirstappwithnavigation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.e07myfirstappwithnavigation.databinding.FragmentSecondBinding


class SecondFragment : Fragment() {

    lateinit var binding: FragmentSecondBinding

    private val args : SecondFragmentArgs by navArgs()

    private var message= ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        message=args.message

        binding.apply {
            receivedData.text=message

            buttonToFirstFragment.setOnClickListener {
                val direction = SecondFragmentDirections.actionSecondFragmentToFirstFragment()
                findNavController().navigate(direction)
            }
        }




    }


}