package com.example.e12golfcoursesclustered

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem


class GolfCourseItem(
    private val position: LatLng,
    private val title: String,
    private val snippet: String,
    private val type: String,
    private val phone: String,
    private val email: String,
    private val webUrl: String
) : ClusterItem {

    override fun getPosition(): LatLng {
        return position
    }

    override fun getTitle(): String {
        return title
    }

    override fun getSnippet(): String {
        return snippet
    }

    fun getType(): String {
        return type
    }

    fun getPhone(): String {
        return phone
    }

    fun getEmail(): String {
        return email
    }

    fun getWebUrl(): String {
        return webUrl
    }
}