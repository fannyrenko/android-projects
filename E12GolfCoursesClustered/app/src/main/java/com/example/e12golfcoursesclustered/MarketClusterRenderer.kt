package com.example.e12golfcoursesclustered

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class MarkerClusterRenderer(
    context: Context?,
    map: GoogleMap?,
    clusterManager: ClusterManager<GolfCourseItem>?
) : DefaultClusterRenderer<GolfCourseItem>(context, map, clusterManager) {

    private val courseTypes: Map<String, Float> = mapOf(
        "?" to BitmapDescriptorFactory.HUE_VIOLET,
        "Etu" to BitmapDescriptorFactory.HUE_BLUE,
        "Kulta" to BitmapDescriptorFactory.HUE_GREEN,
        "Kulta/Etu" to BitmapDescriptorFactory.HUE_YELLOW
    )

    override fun onBeforeClusterItemRendered(item: GolfCourseItem, markerOptions: MarkerOptions) {
        // Get the type of the current GolfCourseItem
        val type = item.getType()

        // Check if the type exists in the courseTypes map
        if (courseTypes.containsKey(type)) {
            // Set the marker color based on the type
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(courseTypes[type]!!))
        } else {
            // Default to blue if the type is not found in the map
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
        }

        super.onBeforeClusterItemRendered(item, markerOptions)
    }
}