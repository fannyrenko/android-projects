package com.example.e12golfcoursesclustered

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.example.e12golfcoursesclustered.databinding.ActivityMapsBinding
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterManager
import org.json.JSONException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var clusterManager: ClusterManager<GolfCourseItem>
    private lateinit var markerClusterRenderer: MarkerClusterRenderer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Initialize ClusterManager and MarkerClusterRenderer
        clusterManager = ClusterManager(this, mMap)
        markerClusterRenderer = MarkerClusterRenderer(this, mMap, clusterManager)
        clusterManager.renderer = markerClusterRenderer

        // Set listeners for ClusterManager
        mMap.setOnCameraIdleListener(clusterManager)
        mMap.setOnMarkerClickListener(clusterManager)

        loadData()

    }

    private fun loadData() {
        val url = "https://ptm.fi/materials/golfcourses/golf_courses.json"

        val queue = Volley.newRequestQueue(this)

        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null,
            { response ->
                // JSON loaded successfully
                try {
                    val golfCourses = response.getJSONArray("courses")

                    // Create a list to hold GolfCourseItem instances
                    val golfCourseItems = mutableListOf<GolfCourseItem>()

                    // Loop through all objects
                    for (i in 0 until golfCourses.length()) {
                        // Get course data
                        val course = golfCourses.getJSONObject(i)
                        val lat = course["lat"].toString().toDouble()
                        val lng = course["lng"].toString().toDouble()
                        val latLng = LatLng(lat, lng)
                        val type = course["type"].toString()
                        val title = course["course"].toString()
                        val address = course["address"].toString()
                        val phone = course["phone"].toString()
                        val email = course["email"].toString()
                        val webUrl = course["web"].toString()

                        // Create GolfCourseItem and add it to the list
                        val golfCourseItem = GolfCourseItem(latLng, title, address, type, phone, email, webUrl)
                        golfCourseItems.add(golfCourseItem)
                    }

                    // Add GolfCourseItems to ClusterManager
                    clusterManager.addItems(golfCourseItems)

                    // Move camera to a suitable position
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(65.5, 26.0), 5.0F))
                } catch (e: JSONException) {
                    Log.e("GolfCourses", "Error parsing JSON", e)
                }
            },
            { error ->
                // Error loading JSON
                Log.e("GolfCourses", "Error loading JSON", error)
            }
        )

        queue.add(jsonObjectRequest)
    }

    internal inner class CustomInfoWindowAdapter : GoogleMap.InfoWindowAdapter {
        private val contents: View = LayoutInflater.from(this@MapsActivity).inflate(R.layout.info_window, null)

        override fun getInfoContents(marker: Marker): View {
            val titleTextView = contents.findViewById<TextView>(R.id.titleTextView)
            val addressTextView = contents.findViewById<TextView>(R.id.addressTextView)
            val phoneTextView = contents.findViewById<TextView>(R.id.phoneTextView)
            val emailTextView = contents.findViewById<TextView>(R.id.emailTextView)
            val webTextView = contents.findViewById<TextView>(R.id.webTextView)

            if (marker.tag is GolfCourseItem) {
                val golfCourseItem: GolfCourseItem = marker.tag as GolfCourseItem
                titleTextView.text = golfCourseItem.getTitle()
                addressTextView.text = golfCourseItem.getSnippet()
                phoneTextView.text = golfCourseItem.getPhone()
                emailTextView.text = golfCourseItem.getEmail()
                webTextView.text = golfCourseItem.getWebUrl()
            }

            return contents
        }


        override fun getInfoWindow(marker: Marker): View? {
            return null
        }
    }



}


