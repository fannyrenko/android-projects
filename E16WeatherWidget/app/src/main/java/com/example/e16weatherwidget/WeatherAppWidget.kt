package com.example.e16weatherwidget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.RemoteViews
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.AppWidgetTarget
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter


class WeatherAppWidget : AppWidgetProvider() {
    val API_LINK: String = "https://api.openweathermap.org/data/2.5/weather?"
    val API_ICON: String = "https://openweathermap.org/img/w/"
    val API_KEY: String = "2414d8bdb687934853eb50ec3a7a91a1"
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        for (appWidgetId in appWidgetIds) {

            // Create an Intent to launch MainActivity
            val intent = Intent(context, MainActivity::class.java)

            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)

            // Get the layout for the App Widget and attach an on-click listener
            val views = RemoteViews(context.packageName, R.layout.weather_app_widget)
            views.setOnClickPendingIntent(R.id.cityTextView, pendingIntent)

            // create intent
            val refreshIntent = Intent(context, WeatherAppWidget::class.java)
            refreshIntent.action = "com.example.weatherwidget.REFRESH"
            refreshIntent.putExtra("appWidgetId", appWidgetId)

            // create pending intent
            val refreshPendingIntent = PendingIntent.getBroadcast(context, 0, refreshIntent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
            // set pending intent to refresh image view
            views.setOnClickPendingIntent(R.id.refreshImage, refreshPendingIntent)


            // Load weather forecast
            loadWeatherForecast("Jyvaskyla", context, views, appWidgetId, appWidgetManager)

        }
    }

    private fun loadWeatherForecast(
        city: String,
        context: Context,
        views: RemoteViews,
        appWidgetId: Int,
        appWidgetManager: AppWidgetManager
    ) {
        Log.d("WEATHER", "Loading weather forecast for $city")

        // URL to load forecast
        val url = "$API_LINK&q=$city&APPID=$API_KEY&units=metric"

        // JSON object request with Volley
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, url, null, { response ->
                try {

                    Log.d("WEATHER", "Response: $response")
                    // load OK - parse data from the loaded JSON
                    val mainJSONObject = response.getJSONObject("main")
                    val weatherArray = response.getJSONArray("weather")
                    val firstWeatherObject = weatherArray.getJSONObject(0)

                    // city, condition, temperature
                    val cityName = response.getString("name")
                    val condition = firstWeatherObject.getString("main")
                    val temperature = mainJSONObject.getString("temp") + " °C"

                    // time
                    val weatherTime: String = response.getString("dt")
                    val weatherLong: Long = weatherTime.toLong()
                    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
                    val dt = Instant.ofEpochSecond(weatherLong).atZone(ZoneId.systemDefault()).toLocalDateTime().format(formatter).toString()

                    // Set text values to TextViews
                    views.setTextViewText(R.id.cityTextView, cityName)
                    views.setTextViewText(R.id.condTextView, condition)
                    views.setTextViewText(R.id.tempTextView, temperature)
                    views.setTextViewText(R.id.timeTextView, dt)

                    // AppWidgetTarget will be used with Glide - image target view
                    val awt: AppWidgetTarget = object : AppWidgetTarget(
                        context.applicationContext,
                        R.id.iconImageView,
                        views,
                        appWidgetId
                    ) {}

                    val weatherIcon = firstWeatherObject.getString("icon")
                    val iconUrl = "$API_ICON$weatherIcon.png"

                    // Use Glide to load and set the weather icon
                    Glide.with(context)
                        .asBitmap()
                        .load(iconUrl)
                        .into(awt)

                    // Update the widget
                    appWidgetManager.updateAppWidget(appWidgetId, views)

                    Log.d("WEATHER", "Weather data loaded successfully")

                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.e("WEATHER", "Error loading weather data: $e")
                }
            },
            { error -> Log.e("WEATHER", "Volley error: $error") })

        // Start loading data with Volley
        val queue = Volley.newRequestQueue(context)
        queue.add(jsonObjectRequest)
    }

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        // got a new action, check if it is refresh action
        if (intent.action == "com.example.weatherwidget.REFRESH") {
            // get manager
            val appWidgetManager = AppWidgetManager.getInstance(context.applicationContext)
            // get appWidgetId
            val appWidgetId = intent.extras!!.getInt("appWidgetId")

            // Retrieve the existing RemoteViews instance
            val views = RemoteViews(context.packageName, R.layout.weather_app_widget)

            // load data again using the existing RemoteViews
            loadWeatherForecast("Jyvaskyla", context, views, appWidgetId, appWidgetManager)
        }
    }
}