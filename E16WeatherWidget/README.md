# Exercise 16 - Weather Widget

## Exercise description
In this exercise you will made a weather forecast widget using Android Studio and Kotlin programming language. Application will load weather forecast data from the Open Weather Map. Loaded weather forecast data will be shown in Android device home screen.

## Screenshots from the Android Emulator
### Weather Widget

<img src="./screenshot_1.png" height='600'>

