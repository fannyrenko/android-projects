# Exercise 10 - Show your favourite cities in a Google Maps

## Exercise description

The purpose of this exercise is to learn how to use Google Maps in Android application.
Requirements

- Use any Maps API in your Android application
- Show 5 favourite cities in a Map with a Markers
- City name will be shown when a marker is pressed

## Screenshots from Android Emulator
### All markers

<img src="./all_markers.png" height='600'>

### Marker in Turku

<img src="./marker_in_turku.png" height='600'>


### Marker in Jyväskylä

<img src="./marker_in_jyvaskyla.png" height='600'>