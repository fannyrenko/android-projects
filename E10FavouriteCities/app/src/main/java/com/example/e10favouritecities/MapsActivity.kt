package com.example.e10favouritecities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.example.e10favouritecities.databinding.ActivityMapsBinding

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        // Add a markers
        val helsinki = LatLng(60.192059, 24.945831)
        val tampere = LatLng(61.49911, 23.78712)
        val turku = LatLng(60.454510, 22.264824)
        val jyvaskyla = LatLng(62.24, 25.72)
        val heinola = LatLng(61.20564,26.03811)

        mMap.addMarker(MarkerOptions().position(helsinki).title("Helsinki"))
        mMap.addMarker(MarkerOptions().position(tampere).title("Tampere"))
        mMap.addMarker(MarkerOptions().position(turku).title("Turku"))
        mMap.addMarker(MarkerOptions().position(jyvaskyla).title("Jyväskylä"))
        mMap.addMarker(MarkerOptions().position(heinola).title("Heinola"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tampere))
    }
}