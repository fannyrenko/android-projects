package com.example.e08employeesapp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView:RecyclerView
    private lateinit var employeesAdapter: EmployeesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Find recyclerView from the layout
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        // Use LinearManager as a layout manager for recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Set an initial empty adapter
        employeesAdapter = EmployeesAdapter(JSONArray())
        recyclerView.adapter = employeesAdapter

        // start loading JSON data
        loadJSON()
    }

    private fun loadJSON() {
        // Instantiate the RequestQueue
        val queue = Volley.newRequestQueue(this)
        // URL to JSON data - remember use your own URL here
        val url = "https://student.labranet.jamk.fi/~AB3835/android/android_data.json"
        // Create request and listeners
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            { response ->
                // Successfully loaded JSON data
                val employees = response.getJSONArray("employees")
                // Update the adapter with the loaded data
                employeesAdapter.updateData(employees)
            },
            { error ->
                Log.d("JSON", "Error loading JSON: ${error.message}")
            }
        )
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest)
    }
}