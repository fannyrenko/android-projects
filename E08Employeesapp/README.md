# Exercise 08 - Show employees in a RecyclerView
## Exercise description
This exercise teaches how to load JSON data from the web and show loaded data in a RecyclerView. Application will show all employees in a list and detailed information in different activity.

## Screenshots from the Android Emulator
### Viewholder

<img src="./viewholder.png" height='600'>

### Employees Activity Layout

<img src="./employeesActivityLayout.png" height='600'>